-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table siantri_rs.tbl_antrian
CREATE TABLE IF NOT EXISTS `tbl_antrian` (
  `id_antrian` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) DEFAULT NULL,
  `nama_pasien` varchar(50) DEFAULT NULL,
  `tanggal_antri` date DEFAULT NULL,
  `jam_antri` time DEFAULT NULL,
  `no_antrian` int(11) DEFAULT '1',
  `id_poli` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_antrian`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- Dumping data for table siantri_rs.tbl_antrian: ~0 rows (approximately)

-- Dumping structure for table siantri_rs.tbl_dokter
CREATE TABLE IF NOT EXISTS `tbl_dokter` (
  `id_dokter` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dokter` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `id_poli` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_dokter`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table siantri_rs.tbl_dokter: ~0 rows (approximately)
INSERT INTO `tbl_dokter` (`id_dokter`, `nama_dokter`, `alamat`, `no_hp`, `id_poli`) VALUES
	(1, 'Pak Dokter', 'Sidoarjo', '088888888888', NULL);

-- Dumping structure for table siantri_rs.tbl_jadwal_dokter
CREATE TABLE IF NOT EXISTS `tbl_jadwal_dokter` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `id_dokter` int(11) NOT NULL DEFAULT '0',
  `id_poli` int(11) NOT NULL DEFAULT '0',
  `hari` varchar(50) DEFAULT NULL,
  `jam_mulai` varchar(50) DEFAULT NULL,
  `jam_akhir` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table siantri_rs.tbl_jadwal_dokter: ~2 rows (approximately)
INSERT INTO `tbl_jadwal_dokter` (`id_jadwal`, `id_dokter`, `id_poli`, `hari`, `jam_mulai`, `jam_akhir`) VALUES
	(1, 1, 0, 'Senin', '08:00', '16:00'),
	(2, 1, 0, 'Selasa', '08:00', '16:00');

-- Dumping structure for table siantri_rs.tbl_pasien
CREATE TABLE IF NOT EXISTS `tbl_pasien` (
  `id_pasien` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) NOT NULL,
  `nama_pasien` varchar(200) DEFAULT NULL,
  `tempat_lahir` varchar(200) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `gol_darah` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` text,
  PRIMARY KEY (`id_pasien`),
  UNIQUE KEY `nik` (`nik`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table siantri_rs.tbl_pasien: ~2 rows (approximately)
INSERT INTO `tbl_pasien` (`id_pasien`, `nik`, `nama_pasien`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `gol_darah`, `agama`, `no_hp`, `alamat`, `username`, `password`) VALUES
	(1, '1', 'Admin', 'Sidoarjo', NULL, 'L', 'A', 'islam', NULL, 'sidoarjo', 'admin', '21232f297a57a5a743894a0e4a801fc3'),
	(2, '123456789', 'Yola', 'Lamongan', '1999-02-01', 'L', 'A', 'Islam', NULL, 'gedangan', 'yola', 'eeb750d17b0e73308cf36acce5ae38e4');

-- Dumping structure for table siantri_rs.tbl_pendaftaran
CREATE TABLE IF NOT EXISTS `tbl_pendaftaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pasien` varchar(50) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(50) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `id_poli` int(11) DEFAULT NULL,
  `no_antrian` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_poli` (`id_poli`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table siantri_rs.tbl_pendaftaran: ~1 rows (approximately)
INSERT INTO `tbl_pendaftaran` (`id`, `nama_pasien`, `nik`, `jenis_kelamin`, `tanggal`, `id_poli`, `no_antrian`, `status`) VALUES
	(9, 'LIONEL ANDREAS MESSI', '12345', 'L', '2023-02-16', 1, 11, '1');

-- Dumping structure for table siantri_rs.tbl_pengguna
CREATE TABLE IF NOT EXISTS `tbl_pengguna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table siantri_rs.tbl_pengguna: ~0 rows (approximately)
INSERT INTO `tbl_pengguna` (`id`, `username`, `password`, `nama`) VALUES
	(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin');

-- Dumping structure for table siantri_rs.tbl_poli
CREATE TABLE IF NOT EXISTS `tbl_poli` (
  `id_poli` int(11) NOT NULL AUTO_INCREMENT,
  `nama_poli` varchar(50) DEFAULT NULL,
  `antrian_saat_ini` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_poli`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table siantri_rs.tbl_poli: ~3 rows (approximately)
INSERT INTO `tbl_poli` (`id_poli`, `nama_poli`, `antrian_saat_ini`) VALUES
	(1, 'Poli Gigi', 0),
	(2, 'Poli THT', 0),
	(3, 'poli bedah', 0);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
