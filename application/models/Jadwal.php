<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Jadwal extends CI_Model{
		public function get_all()
		{
			$sql = "SELECT j.*,d.nama_dokter FROM tbl_jadwal_dokter j LEFT JOIN tbl_dokter d ON j.id_dokter=d.id_dokter ORDER BY j.id_poli ASC,d.nama_dokter ASC,j.hari ASC, j.jam_mulai ASC";
			return $this->db->query($sql);
		}

		public function detail($id_jadwal)
		{
			return $this->db->get_where('tbl_jadwal_dokter',['id_jadwal'=>$id_jadwal]);
		}
		public function get_where($where=array())
		{
			return $this->db->get_where('tbl_jadwal_dokter',$where);
		}

		public function insert($data=array())
		{
			return $this->db->insert('tbl_jadwal_dokter', $data);
		}

		public function update($data=array(), $id_jadwal)
		{
			$this->db->where('id_jadwal', $id_jadwal);
			return $this->db->update('tbl_jadwal_dokter', $data);
		}

		public function delete($id_jadwal)
		{
			$val = array(
				'id_jadwal' => $id_jadwal
			);
			return $this->db->delete('tbl_jadwal_dokter', $val);
		}
	}