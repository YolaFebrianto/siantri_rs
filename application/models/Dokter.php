<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dokter extends CI_Model{
		public function get_all()
		{
			return $this->db->get('tbl_dokter');
		}

		public function detail($id_dokter)
		{
			return $this->db->get_where('tbl_dokter',['id_dokter'=>$id_dokter]);
		}

		public function insert($data=array())
		{
			return $this->db->insert('tbl_dokter', $data);
		}

		public function update($data=array(), $id_dokter)
		{
			$this->db->where('id_dokter', $id_dokter);
			return $this->db->update('tbl_dokter', $data);
		}

		public function delete($id_dokter)
		{
			$val = array(
				'id_dokter' => $id_dokter
			);
			return $this->db->delete('tbl_dokter', $val);
		}
	}