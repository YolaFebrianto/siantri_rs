<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pasien extends CI_Model{
		public function login($username,$password)
		{
	        $where = [
	            'username' => $username,
	            'password' => md5($password)
	        ];
	        return $this->db->get_where('tbl_pasien',$where);
		}
		public function detail($username)
		{
			return $this->db->get_where('tbl_pasien',['username'=>$username]);
		}
		public function detail_id($id_pasien=0)
		{
			return $this->db->get_where('tbl_pasien',['id_pasien'=>$id_pasien]);
		}

		public function get_all()
		{
			return $this->db->get('tbl_pasien');
		}

		public function insert($data=array())
		{
			return $this->db->insert('tbl_pasien', $data);
		}

		public function update($data=array(), $id_pasien)
		{
			$this->db->where('id_pasien', $id_pasien);
			return $this->db->update('tbl_pasien', $data);
		}

		public function delete($id_pasien)
		{
			$val = array(
				'id_pasien' => $id_pasien
			);
			return $this->db->delete('tbl_pasien', $val);
		}
	}