<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Jadwal');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		$head['title'] 	= 'Data Jadwal';
		$data['jadwal']	= $this->Jadwal->get_all()->result();
		$this->load->view('user/templates/header',$head);
		$this->load->view('user/jadwal/index',$data);
		$this->load->view('user/templates/footer');
	}
}