<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AntrianController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Antrian');
		$this->load->model('Pasien');
		$this->load->model('Poli');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		$head['title'] 	= 'Beranda';
		$username 		= @$this->session->userdata('username');
		$data['user'] 	= $this->Pasien->detail($username)->row_array();
		$user_id 		= @$data['user']['id'];
		$data['isi'] 	= array();
		$data['poli']	= $this->Poli->get_all()->result();
		$this->load->view('user/templates/header');
		$this->load->view('user/antrian/index',$data);
		$this->load->view('user/templates/footer');
	}
	// public function tambah($id_poli=0){
	// 	$nik_login = @$this->session->userdata('nik_login');
	// 	$query_antrian = $this->Antrian->get_total_antrian($id_poli)->row_array();
	// 	$get_no_antrian = @$query_antrian['total_antrian'];
	// 	// NO ANTRIAN SAYA = TOTAL ANTRIAN SEBELUM SAYA + 1
	// 	if ($get_no_antrian>=0) {
	// 		$get_no_antrian = $get_no_antrian+1;
	// 	} else {
	// 		$get_no_antrian = 0;
	// 	}
	// 	$data = [
	// 		'nik' 			=> $nik_login,
	// 		'tanggal_antri' => date('Y-m-d'),
	// 		'jam_antri'		=> date('H:i:s'),
	// 		'no_antrian' 	=> $get_no_antrian,
	// 		'id_poli' 		=> $id_poli,
	// 	];
	// 	try {
	// 		$cek = $this->Antrian->insert($data);
	// 		$this->session->set_flashdata('info','Data Antrian Berhasil Ditambahkan!');
	// 	} catch (Exception $e) {
	// 		$this->session->set_flashdata('danger','Data Antrian Gagal Ditambahkan!');
	// 	}
	// 	redirect('pasien');
	// }
	public function post_tambah(){
		// $jam_antri = $this->input->post('jam_antri');
		$jam_antri = date('H:i');
		$data = [
			'id_poli'		=> $this->input->post('id_poli'),
			'nik'			=> $this->input->post('nik'),
			'tanggal_antri'	=> $this->input->post('tanggal_antri'),
			'jam_antri'		=> $jam_antri,
			'no_antrian'	=> $this->input->post('no_antrian'),
		];
		try {
			$cek = $this->Antrian->insert($data);
			$this->session->set_flashdata('info','Antrian Berhasil Di ambil');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Antrian Gagal DIambil!');
		}
		redirect('antrian/index');
	}
	public function get_no_antrian()
	{
		$get_id_poli = $_GET['id_poli'];
		$get_tanggal_antri = '';
		if (!empty($_GET['tanggal_antri'])) {
			$get_tanggal_antri = date('Y-m-d',strtotime($_GET['tanggal_antri']));
		}
		$query_antrian = $this->Antrian->get_total_antrian_harian($get_id_poli,$get_tanggal_antri)->row_array();
		$get_no_antrian = @$query_antrian['total_antrian'];
		// NO ANTRIAN SAYA = TOTAL ANTRIAN SEBELUM SAYA + 1
		if ($get_no_antrian>=0) {
			$get_no_antrian = $get_no_antrian+1;
		} else {
			$get_no_antrian = 0;
		}
		echo $get_no_antrian;
	}
	public function cek_antrian_sekarang()
	{
		$id_poli = 0;
		if (isset($id_poli)) {
			$id_poli = $_GET['id_poli'];
		}
		$data_poli = $this->Poli->detail($id_poli)->row_array();
		echo @$data_poli['antrian_saat_ini'];
	}
}