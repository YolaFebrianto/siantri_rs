<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AntrianController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Antrian');
		$this->load->model('Poli');
		date_default_timezone_set('Asia/Jakarta');

		if (empty(@$this->session->userdata('username'))) {
			redirect('admin');
		}
	}
	public function index()
	{
		$head['title'] 	= 'Data Antrian';
		$data['antrian']= $this->Antrian->get_all()->result();
		$this->load->view('admin/templates/header',$head);
		$this->load->view('admin/antrian/index',$data);
		$this->load->view('admin/templates/footer');
	}
	public function form_tambah(){
		$head['title'] = 'Form Tambah Data Antrian';
		$data['isi'] = array();
		$data['poli'] = $this->Poli->get_all()->result();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/antrian/form_tambah',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_tambah(){
		$tanggal_antri = date('Y-m-d');
		$jam_antri = date('H:i:s');
		$data = [
			'nik'			=> $this->input->post('nik'),
			'tanggal_antri'	=> $tanggal_antri,
			'jam_antri'		=> $jam_antri,
			'no_antrian'	=> $this->input->post('no_antrian'),
			'id_poli'		=> $this->input->post('id_poli'),
		];
		try {
			$cek = $this->Antrian->insert($data);
			$this->session->set_flashdata('info','Data Antrian Berhasil Ditambahkan!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Antrian Gagal Ditambahkan!');
		}
		redirect('admin/antrian/index');
	}
	public function form_edit($id_antrian){
		$head['title'] 	 = 'Form Edit Data Antrian';
		$data['isi'] 	 = $this->Antrian->detail($id_antrian)->row_array();
		$data['poli'] = $this->Poli->get_all()->result();
		$data['id_antrian'] = $id_antrian;
		$this->load->view('admin/templates/header');
		$this->load->view('admin/antrian/form_edit',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_edit($id_antrian){
		$tanggal_antri='';
		$tanggal_antri_post=$this->input->post('tanggal_antri');
		if (!empty($tanggal_antri_post) AND $tanggal_antri_post!='0000-00-00') {
			$tanggal_antri = date('Y-m-d',strtotime($this->input->post('tanggal_antri')));
		}
		$jam_antri='';
		$jam_antri_post=$this->input->post('jam_antri');
		if (!empty($jam_antri_post) AND $jam_antri_post!='00:00:00') {
			$jam_antri = date('H:i:s',strtotime($this->input->post('jam_antri')));
		}
		$data = [
			'nik'			=> $this->input->post('nik'),
			'tanggal_antri'	=> $tanggal_antri,
			'jam_antri'		=> $jam_antri,
			'no_antrian'	=> $this->input->post('no_antrian'),
			'id_poli'		=> $this->input->post('id_poli'),
		];
		try {
			$cek = $this->Antrian->update($data, $id_antrian);
			$this->session->set_flashdata('info','Data Antrian Berhasil Diubah!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Antrian Gagal Diubah!');
		}
		redirect('admin/antrian/index');
	}
	public function hapus($id_antrian){
		try {
			$cek = $this->Antrian->delete($id_antrian);
			$this->session->set_flashdata('info', 'Data Antrian Berhasil Dihapus!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger', 'Data Antrian Gagal Dihapus!');
		}
		redirect('admin/antrian/index');
	}
	public function get_no_antrian()
	{
		$id_poli = 0;
		if (isset($_GET['id_poli'])) {
			$id_poli = $_GET['id_poli'];
		}
		$query_antrian = $this->Antrian->get_total_antrian($id_poli)->row_array();
		$get_no_antrian = @$query_antrian['total_antrian'];
		// NO ANTRIAN SAYA = TOTAL ANTRIAN SEBELUM SAYA + 1
		if ($get_no_antrian>=0) {
			$get_no_antrian = $get_no_antrian+1;
		} else {
			$get_no_antrian = 0;
		}
		echo $get_no_antrian;
	}
	public function next(){
		$id_poli = 0;
		if (isset($id_poli)) {
			$id_poli = $_GET['id_poli'];
		}
		try {
			$cek = $this->Antrian->next($id_poli);
			// $this->session->set_flashdata('info','Data Jadwal Berhasil Diubah!');
		} catch (Exception $e) {
			// $this->session->set_flashdata('danger','Data Jadwal Gagal Diubah!');
		}
		$data_poli = $this->Poli->detail($id_poli)->row_array();
		echo @$data_poli['antrian_saat_ini'];
		// redirect('admin');
	}
	public function reset(){
		$id_poli = 0;
		if (isset($id_poli)) {
			$id_poli = $_GET['id_poli'];
		}
		try {
			$cek = $this->Antrian->reset($id_poli);
			// $this->session->set_flashdata('info','Data Jadwal Berhasil Diubah!');
		} catch (Exception $e) {
			// $this->session->set_flashdata('danger','Data Jadwal Gagal Diubah!');
		}
		$data_poli = $this->Poli->detail($id_poli)->row_array();
		echo @$data_poli['antrian_saat_ini'];
		// redirect('admin');
	}
}