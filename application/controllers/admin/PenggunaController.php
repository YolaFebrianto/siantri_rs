<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenggunaController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Pengguna');
		$this->load->model('Poli');
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index()
	{
		if ($this->session->userdata('username') != '') {
			$head['title'] 	= 'Beranda Admin';
			$username 		= $this->session->userdata('username');
			$data['user'] 	= $this->Pengguna->detail($username)->row_array();
			$user_id 		= @$data['user']['id'];
			$data['isi'] 	= array();
			$data['poli']	= $this->Poli->get_all()->result();
			$this->load->view('admin/templates/header',$head);
			$this->load->view('admin/pengguna/index',$data);
			$this->load->view('admin/templates/footer');
		} else {
			$this->load->view('admin/pengguna/login');
		}
	}
	public function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
        $cek = $this->Pengguna->login($username,$password)->row_array();
		if ($cek != null) {
			$this->session->set_userdata('username',$username);
			redirect('admin');
		} else {
			$this->session->set_flashdata('error','Username atau Password Salah!');
			redirect('admin');
		}	
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('admin');
	}
}
