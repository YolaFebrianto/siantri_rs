<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PoliController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Poli');
		date_default_timezone_set('Asia/Jakarta');

		if (empty(@$this->session->userdata('username'))) {
			redirect('admin');
		}
	}
	public function index()
	{
		$head['title'] 	= 'Data Poli';
		$data['poli']	= $this->Poli->get_all()->result();
		$this->load->view('admin/templates/header',$head);
		$this->load->view('admin/poli/index',$data);
		$this->load->view('admin/templates/footer');
	}
	public function form_tambah(){
		$head['title'] = 'Form Tambah Data Poli';
		$data['isi'] = array();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/poli/form_tambah',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_tambah(){
		$data = [
			'nama_poli' => $this->input->post('nama_poli'),
		];
		try {
			$cek = $this->Poli->insert($data);
			$this->session->set_flashdata('info','Data Poli Berhasil Ditambahkan!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Poli Gagal Ditambahkan!');
		}
		redirect('admin/poli/index');
	}
	public function form_edit($id_poli){
		$head['title'] 	 = 'Form Edit Data Poli';
		$data['isi'] 	 = $this->Poli->detail($id_poli)->row_array();
		$data['id_poli'] = $id_poli;
		$this->load->view('admin/templates/header');
		$this->load->view('admin/poli/form_edit',$data);
		$this->load->view('admin/templates/footer');
	}
	public function post_edit($id){
		$data = [
			'nama_poli' => $this->input->post('nama_poli'),
		];
		try {
			$cek = $this->Poli->update($data, $id);
			$this->session->set_flashdata('info','Data Poli Berhasil Diubah!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger','Data Poli Gagal Diubah!');
		}
		redirect('admin/poli/index');
	}
	public function hapus($id){
		try {
			$cek = $this->Poli->delete($id);
			$this->session->set_flashdata('info', 'Data Poli Berhasil Dihapus!');
		} catch (Exception $e) {
			$this->session->set_flashdata('danger', 'Data Poli Gagal Dihapus!');
		}
		redirect('admin/poli/index');
	}
}