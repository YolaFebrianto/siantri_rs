<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SiAntri RS</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?=base_url();?>template/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?=base_url();?>template/bootstrap/css/font-awesome.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?=base_url();?>template/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?=base_url();?>template/dist/css/skins/_all-skins.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?=base_url();?>template/plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?=base_url();?>template/plugins/morris/morris.css">
	<style type="text/css">
		div.disclaimer{
		    display: none;
		}
	</style>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
	<header class="main-header">
		<!-- Logo -->
		<a href="<?php echo base_url(); ?>" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>ARS</b></span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>Si Antri RS</b></span>
		</a>

		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>
			<!-- Navbar Right Menu -->
			<?php if(!empty($this->session->userdata('username'))): ?>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li>
						<a href="#"><?php echo @get_pasien()['username'];?></a>
					</li>
					<li>
						<a href="<?=base_url('pasien/logout');?>"><span class="fa fa-sign-out"></span> Logout</a>
					</li>
				</ul>
			</div>
			<?php endif; ?>
		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="<?=base_url();?>template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
				</div>
				<?php if(!empty($this->session->userdata('username'))): ?>
				<div class="pull-left info">
					<p><?php echo @get_pasien()['username'];?></p>
					<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			    <?php else: ?>
				<div class="pull-left info">
					<p>Guest</p>
					<a href="#"><i class="fa fa-circle text-danger"></i> Offline</a>
				</div>
			    <?php endif; ?>
			</div>
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu">
				<li <?php echo (empty($this->uri->segment(1)) OR ($this->uri->segment(1)=='pasien' && $this->uri->segment(2)=='index') )?'class="active"':'';?>>
					<a href="<?php echo base_url(); ?>">
						<i class="fa fa-dashboard"></i> <span>HOME</span>
					</a>
				</li>
				<li <?php echo($this->uri->segment(1)=='antrian')?'class="active"':'';?>>
					<a href="<?php echo base_url('antrian'); ?>">
						<i class="fa fa-tasks"></i> <span>Antrian</span>
					</a>
				</li>
				<li <?php echo($this->uri->segment(1)=='jadwal')?'class="active"':'';?>>
					<a href="<?php echo base_url('jadwal'); ?>">
						<i class="fa fa-calendar"></i> <span>Jadwal Dokter</span>
					</a>
				</li>
				<?php if(!empty($this->session->userdata('username'))): ?>
				<li <?php echo($this->uri->segment(1)=='pasien'&&$this->uri->segment(2)=='profil')?'class="active"':'';?>>
					<a href="<?php echo base_url('pasien/profil'); ?>">
						<i class="fa fa-user"></i> <span>Profil</span>
					</a>
				</li>
				<?php endif; ?>
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<?php
					if (!empty($title)) {
						echo $title;
					} else {
						echo 'Dashboard';
					}
				?>
				<small></small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">
				<?php
					if (!empty($title)) {
						echo $title;
					} else {
						echo 'Dashboard';
					}
				?>
				</li>
			</ol>
		</section>
		<section class="content">
			<div class="box box-primary">
				<div class="box-body">
					<?php if($this->session->flashdata('info') != null): ?>
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-info"></i> Alert!</h4>
						<?=$this->session->flashdata('info');?>
					</div>
					<?php endif; ?>	
					<?php if($this->session->flashdata('danger') != null): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-ban"></i> Alert!</h4>
						<?=$this->session->flashdata('danger');?>
					</div>
					<?php endif; ?>