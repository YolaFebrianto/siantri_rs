<div class="row">
	<div class="col-md-6">
		<!-- <div class="form-group">
			<label>Username :</label>
			<input type="text" name="username" class="form-control" readonly required value="<?php //echo @get_pasien()['username'];?>">
		</div> -->
		<!-- <div class="form-group"> -->
			<!-- <label>Password :</label> -->
			<input type="hidden" name="text" class="" readonly required value="<?php echo @get_pasien()['password'];?>">	
		<!-- </div> -->
		<div class="form-group">
			<label>NIK :</label>
			<input type="text" name="nik" class="form-control" readonly required value="<?php echo @get_pasien()['nik'];?>">	
		</div>
		<div class="form-group">
			<label>Nama Pasien :</label>
			<input type="text" name="nama_pasien" class="form-control" readonly required max="200" value="<?php echo @get_pasien()['nama_pasien'];?>">	
		</div>
		<div class="form-group">
			<label>Tempat Lahir :</label>
			<input type="text" name="tempat_lahir" class="form-control" readonly value="<?php echo @get_pasien()['tempat_lahir'];?>">	
		</div>
		<div class="form-group">
			<label>Tanggal Lahir :</label>
			<input type="date" name="tgl_lahir" class="form-control" readonly value="<?php echo @get_pasien()['tgl_lahir'];?>">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Jenis Kelamin :</label>
			<input type="text" name="jenis_kelamin" class="form-control" readonly value="<?php echo @get_pasien()['jenis_kelamin'];?>">
		</div>
		<div class="form-group">
			<label>Gol. Darah :</label>
			<input type="text" name="gol_darah" class="form-control" readonly value="<?php echo @get_pasien()['gol_darah'];?>">	
		</div>
		<div class="form-group">
			<label>Agama :</label>
			<input type="text" name="agama" class="form-control" readonly value="<?php echo @get_pasien()['agama'];?>">	
		</div>
		<div class="form-group">
			<label>No. HP :</label>
			<input type="text" name="no_hp" class="form-control" readonly required value="<?php echo @get_pasien()['no_hp'];?>">	
		</div>
		<div class="form-group">
			<label>Alamat :</label>
			<textarea rows="3" name="alamat" class="form-control" readonly><?php echo @get_pasien()['alamat'];?></textarea>
		</div>
	</div>
</div>