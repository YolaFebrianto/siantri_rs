<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-6">
		FORM PENDAFTARAN
		<br><br>
		<?= form_open('antrian/post_tambah'); ?>
			<div class="form-group">
				<label>Poli :</label>
				<select name="id_poli" class="form-control" id="pilih-poli" required onchange="getNoAntrian()">
					<option value="" selected disabled>-- Pilih --</option>
					<?php if (!empty($poli)): ?>
					<?php foreach($poli as $v): ?>
						<?php if (!empty($poli_pilihan) AND $poli_pilihan==$v->id_poli): ?>
						<option value="<?php echo $v->id_poli; ?>" selected><?php echo $v->nama_poli; ?></option>
						<?php else: ?>
						<option value="<?php echo $v->id_poli; ?>"><?php echo $v->nama_poli; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Nama Pasien :</label>
				<input type="text" name="nama" class="form-control" readonly value="<?php echo @get_pasien()['nama_pasien'];?>" required>
			</div>
			<div class="form-group">
				<label>No. Bpjs :</label>
				<input type="text" name="nik" class="form-control" value="<?php echo @get_pasien()['nik']; ?>" readonly required>
			</div>
			<div class="form-group">
				<label>Tanggal :</label>
				<input type="date" name="tanggal_antri" class="form-control" id="tanggal-antrian" value="<?php echo date('Y-m-d'); ?>" required onchange="getNoAntrian()">
			</div>
			<!-- <div class="form-group">
				<label>Jam :</label>
				<input type="time" name="jam_antri" class="form-control">
			</div> -->
			
			<div class="form-group">
				<label>No. Antrian :</label>
				<?php
				$antrian=1;
				if (!empty($poli_pilihan)) {
					$get_no_antrian=@get_antrian($poli_pilihan)['total_antrian'];
					if (!empty($get_no_antrian) AND $get_no_antrian>=0) {
						$antrian = $get_no_antrian+1;
					} else {
						$antrian = 1;
					}
				}
				?>
				<?php //var_dump($antrian);die(); ?>
				<input type="number" name="no_antrian" class="form-control" value="<?php echo $antrian;  ?>" readonly id="no-antrian" required>
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('pasien/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>
<!-- /.row -->
</section>
<!-- /.content -->
<script src="<?=base_url();?>template/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript">
	function getNoAntrian(){
        $.ajax({
            'method': 'GET',
            'url': '<?php echo base_url('antrian/get_no_antrian');?>',
            'data': {id_poli:$('#pilih-poli').val(), tanggal_antri:$('#tanggal-antrian').val()},
            'success': function(data){
                $('#no-antrian').val(data);
            }
        });
    }
    // $('#pilih-poli').change(getNoAntrian());
    // $('#tanggal-antrian').change(getNoAntrian());
</script>