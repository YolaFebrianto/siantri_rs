<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="row">
		<div class="col-md-12">
			<?php if (count($jadwal)>0) { ?>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Dokter</th>
						<th>Poli</th>
						<th>Hari</th>
						<th>Jam Kerja</th>
					</tr>
				</thead>
				<tbody>
			<?php
				foreach ($jadwal as $k => $v) {
			?>
				<tr>
					<td><?php echo ($k+1); ?></td>
					<td><?php echo $v->nama_dokter; ?></td>
					<td><?php echo getPoli(@$v->id_poli); ?></td>
					<td><?php echo getHari($v->hari); ?></td>
					<td><?php echo $v->jam_mulai.'-'.$v->jam_akhir; ?></td>
				</tr>
			<?php
				}
			?>
				</tbody>
			</table>
			<?php
				} else {
					echo "<h4 style='text-align:center;'><i>Jadwal Masih Kosong!</i></h4>";
				}
			?>
		</div>
	</div>
</section>
