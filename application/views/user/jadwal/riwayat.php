<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="row">
		<div class="col-md-12">
			<?php if (count($riwayat)>0) { ?>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th>Nama Pasien</th>
						<th>Tanggal</th>
						<th>No. BPJS</th>
						<!-- <th>Jenis Kelamin</th> -->
						<th>Tujuan Ke Poli</th>
						<th>Namor Antrian</th>
						<!-- <th>Status</th> -->
					</tr>
				</thead>
				<tbody>
			<?php
				foreach ($riwayat as $k => $v) {
			?>
				<tr>
					<td><?php echo $v->nama; ?></td>
					<td><?php echo $v->tanggal_antri; ?></td>
					<td><?php echo $v->nik; ?></td>
					<!-- <td><?php //if ($v->jenis_kelamin == 'L') {
								//echo "LAKI-LAKI";
						//}//elseif ($v->jenis_kelamin == 'P') {
								//echo "PEREMPUAN";
					//} ?></td> -->
					<td><?php echo $v->id_poli; ?></td>
					<td><?php echo $v->no_antrian; ?></td>
					<td>
						<!-- <?php //if ($v->status == '1'): ?>
							<a href="#" class="btn btn-success" readonly><span class="fa fa-check"></span></a>
						<?php //elseif ($v->status == '0'): ?>
							<a href="<?php //echo base_url('admin/jadwal/riwayat/'.$v->id); ?>" class="btn btn-info"><span class="fa fa-pencil" disabled></span></a>
						<?php //endif; ?> -->
					
				</tr>
			<?php
				}
			?>
				</tbody>
			</table>
			<?php
				} else {
					echo "<h4 style='text-align:center;'><i>Jadwal Masih Kosong!</i></h4>";
				}
			?>
		</div>
	</div>
</section>
