<div class="row">
	<div class="col-md-4">
		<?= form_open('admin/dokter/post_tambah'); ?>
			<div class="form-group">
				<label>Nama Dokter :</label>
				<input type="text" name="nama_dokter" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>No. HP :</label>
				<input type="text" name="no_hp" class="form-control">	
			</div>
			<div class="form-group">
				<label>Alamat :</label>
				<textarea rows="3" name="alamat" class="form-control"></textarea>
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/dokter/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>