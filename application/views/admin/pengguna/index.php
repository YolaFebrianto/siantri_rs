<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<?php foreach($poli as $k=>$data_poli): ?>
	<?php
		$no=$k+1;
		if ($no%4==1) {
			$bgColor='bg-green';
		} else if ($no%4==2) {
			$bgColor='bg-aqua';
		} else if ($no%4==3) {
			$bgColor='bg-yellow';
		} else {
			$bgColor='bg-red';
		}
	?>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="small-box <?php echo $bgColor; ?>">
			<div class="inner">
				<?php //$antrian=@get_antrian($data_poli->id_poli); ?>
				<div class="row" style="text-align:center;">
					<div class="col-md-12">
						<h5>No. Antrian Sekarang</h5>
						<h3 style="font-size:42px;" id="no_antrian_sekarang_teks_<?php echo @$data_poli->id_poli; ?>"><?php echo $data_poli->antrian_saat_ini; ?></h3>
					</div>
					<!-- <div class="col-md-6">
						<h5>Total <br>Antrian</h5>
						<h3 style="font-size:32px;"><?php //echo @$antrian['total_antrian']; ?></h3>
					</div> -->
				</div>
				<p style="text-align:center;margin:0;"><?php echo $data_poli->nama_poli; ?></p>
			</div>
			<div class="icon">
				<i class="fa fa-briefcase"></i>
			</div>
			<div class="small-box-footer">
				<div class="row">
					<div class="col-md-5">
						<button class="btn btn-link" style="color:white;" onclick="resetCal(<?php echo @$data_poli->id_poli;?>)">
							<i class="fa fa-rotate-left"></i> Reset
						</button>
					</div>
					<div class="col-md-7">
						<button class="btn btn-link" style="color:white;" onclick="nextCal(<?php echo @$data_poli->id_poli;?>)">
							Selanjutnya <i class="fa fa-arrow-right"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($no%2==0): ?>
	<div class="clearfix visible-sm-block"></div>
	<?php endif; ?>
	<!-- /.col -->
	<?php endforeach; ?>
</div>
<!-- /.row -->
</section>
<!-- /.content-->
<script src="<?=base_url();?>template/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript">
    function resetCal(id_poli_val){
        $.ajax({
            'method': 'GET',
            'url': '<?php echo base_url('admin/antrian/reset');?>',
            'data': {id_poli:id_poli_val},
            'success': function(data){
                $('#no_antrian_sekarang_teks_'+id_poli_val).html(data);
                // alert(data);
                // console.log('success');
            }
        });
    };
    function nextCal(id_poli_val){
        $.ajax({
            'method': 'GET',
            'url': '<?php echo base_url('admin/antrian/next');?>',
            'data': {id_poli:id_poli_val},
            'success': function(data){
                $('#no_antrian_sekarang_teks_'+id_poli_val).html(data);
                // alert(data);
                // console.log('success');
            }
        });
    };
</script>