<div class="row">
	<div class="col-md-4">
		<?= form_open('admin/pasien/post_edit/'.$id_pasien); ?>
			<div class="form-group">
				<label>NIK :</label>
				<input type="text" name="nik" class="form-control" value="<?php echo $isi['nik'];?>" required>	
			</div>
			<div class="form-group">
				<label>Nama Pasien :</label>
				<input type="text" name="nama_pasien" class="form-control" value="<?php echo $isi['nama_pasien'];?>" required>	
			</div>
			<div class="form-group">
				<label>Tempat Lahir :</label>
				<input type="text" name="tempat_lahir" class="form-control" value="<?php echo $isi['tempat_lahir'];?>" required>	
			</div>
			<div class="form-group">
				<label>Tanggal Lahir :</label>
				<!-- <input type="date" name="tgl_lahir" class="form-control" value="<?php //echo $isi['tgl_lahir'];?>" required> -->
				<div class="input-group date" data-date="" data-date-format="yyyy-mm-dd">
					<input class="form-control input-sm" type="text" name="tgl_lahir" placeholder="Tanggal Lahir" value="<?php echo $isi['tgl_lahir'];?>" readonly required />
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label>Jenis Kelamin :</label>
				<select name="jenis_kelamin" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<option value="L" <?php echo ($isi['jenis_kelamin']=='L')?'selected':''; ?>>Laki- laki</option>
					<option value="P" <?php echo ($isi['jenis_kelamin']=='P')?'selected':''; ?>>Perempuan</option>
				</select>
			</div>
			<div class="form-group">
				<label>Gol. Darah :</label>
				<select name="gol_darah" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<option value="A" <?php echo ($isi['gol_darah']=='A')?'selected':''; ?>>A</option>
					<option value="B" <?php echo ($isi['gol_darah']=='B')?'selected':''; ?>>B</option>
					<option value="O" <?php echo ($isi['gol_darah']=='O')?'selected':''; ?>>O</option>
					<option value="AB" <?php echo ($isi['gol_darah']=='AB')?'selected':''; ?>>AB</option>
				</select>
			</div>
			<div class="form-group">
				<label>Agama :</label>
				<input type="text" name="agama" class="form-control" value="<?php echo $isi['agama'];?>" required>	
			</div>
			<div class="form-group">
				<label>No. HP :</label>
				<input type="text" name="no_hp" class="form-control" value="<?php echo $isi['no_hp'];?>" required>	
			</div>
			<div class="form-group">
				<label>Alamat :</label>
				<textarea rows="3" name="alamat" class="form-control" required><?php echo $isi['alamat'];?></textarea>
			</div>
			<div class="form-group">
				<label>Username :</label>
				<input type="text" name="username" class="form-control" value="<?php echo $isi['username'];?>" required>	
			</div>
			<input type="submit" name="edit" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/pasien/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>
<!-- <script src="<?php //echo base_url();?>template/plugins/jQuery/jquery-2.2.3.min.js"></script> -->
<link rel="stylesheet" href="<?php echo base_url().'template/plugins/datepicker/datepicker3.css'; ?>" type="text/css" />
<script type="text/javascript" src="<?php echo base_url().'template/plugins/datepicker/bootstrap-datepicker.js'; ?>"></script>
<script type="text/javascript">
$().ready(function() {
	//options method for call datepicker
	$(".input-group.date").datepicker({ 
		autoclose: true, 
		todayHighlight: true,
		viewMode: 'date',
		format: 'yyyy-mm-dd',
		changeDate: false
	});
});	
</script>