<div class="row">
	<div class="col-md-4">
		<?= form_open('admin/antrian/post_edit/'.$id_antrian); ?>
			<div class="form-group">
				<label>NIK :</label>
				<input type="text" name="nik" class="form-control" value="<?php echo $isi['nik']; ?>" required>
			</div>
			<div class="form-group">
				<label>No. Antrian :</label>
				<input type="text" name="no_antrian" class="form-control" value="<?php echo $isi['no_antrian']; ?>" required>
			</div>
			<div class="form-group">
				<label>Tanggal Antri :</label>
				<!-- <input type="date" name="tanggal_antri" class="form-control" value="<?php //echo $isi['tanggal_antri']; ?>" required> -->
				<div class="input-group date" data-date="" data-date-format="yyyy-mm-dd">
					<input class="form-control input-sm" type="text" name="tanggal_antri" placeholder="Tanggal Antri" value="<?php echo $isi['tanggal_antri']; ?>" readonly required />
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label>Jam Antri :</label>
				<input type="time" name="jam_antri" class="form-control" value="<?php echo $isi['jam_antri']; ?>" required>
			</div>
			<div class="form-group">
				<label>Poli :</label>
				<select name="id_poli" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<?php foreach($poli as $v): ?>
						<?php if($isi['id_poli']==$v->id_poli): ?>
						<option value="<?php echo $v->id_poli; ?>" selected><?php echo $v->nama_poli; ?></option>
						<?php else: ?>
						<option value="<?php echo $v->id_poli; ?>"><?php echo $v->nama_poli; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
			</div>
			<input type="submit" name="edit" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/antrian/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>
<script src="<?=base_url();?>template/plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url().'template/plugins/datepicker/datepicker3.css'; ?>" type="text/css" />
<script type="text/javascript" src="<?php echo base_url().'template/plugins/datepicker/bootstrap-datepicker.js'; ?>"></script>
<script type="text/javascript">
$().ready(function() {
	//options method for call datepicker
	$(".input-group.date").datepicker({ 
		autoclose: true, 
		todayHighlight: true,
		viewMode: 'date',
		format: 'yyyy-mm-dd',
		changeDate: false
	});
});	
</script>