<div class="row">
	<div class="col-md-4">
		<?= form_open('admin/jadwal/post_tambah'); ?>
			<div class="form-group">
				<label>Nama Dokter :</label>
				<select name="id_dokter" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<?php if(!empty($dokter)): ?>
					<?php foreach($dokter as $v): ?>
					<option value="<?php echo $v->id_dokter; ?>"><?php echo $v->nama_dokter; ?></option>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Poli :</label>
				<select name="id_poli" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<?php if(!empty($poli)): ?>
					<?php foreach($poli as $v): ?>
					<option value="<?php echo $v->id_poli; ?>"><?php echo $v->nama_poli; ?></option>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Hari :</label>
				<!-- <input type="text" name="hari" class="form-control"> -->
				<select name="hari" class="form-control" required>
					<option value="" selected disabled>-- Pilih --</option>
					<option value="1">Senin</option>
					<option value="2">Selasa</option>
					<option value="3">Rabu</option>
					<option value="4">Kamis</option>
					<option value="5">Jumat</option>
					<option value="6">Sabtu</option>
					<option value="7">Minggu</option>
				</select>
			</div>
			<div class="form-group">
				<label>Jam Mulai :</label>
				<input type="time" name="jam_mulai" class="form-control" required>	
			</div>
			<div class="form-group">
				<label>Jam Akhir :</label>
				<input type="time" name="jam_akhir" class="form-control" required>	
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/jadwal/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>