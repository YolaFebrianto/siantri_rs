<div class="row">
	<div class="col-md-4 col-sm-6 col-xs-10">
		<?= form_open('admin/jadwal/post_edit_pasien/'.$id); ?>
			<div class="form-group">
				<label>No. Antrian :</label>
				<?php $x=0; ?>
				<input type="number" name="antri" class="form-control" value="<?php echo $isi['no_antrian']; ?>">	
			</div>
			<div class="form-group">
				<label>Nama Pasien :</label>
				<select name="id_pasien" class="form-control" >
					<option value="" selected disabled>-- Pilih --</option>
					<?php if(!empty($pasien)): ?>
					<?php foreach($pasien as $v): ?>
						<?php if($isi['nama_pasien']==$v->nama_pasien): ?>
					<option value="<?php echo $v->nama_pasien; ?>" selected><?php echo $v->nama_pasien; ?></option>
				<?php else : ?>
					<option value="<?php echo $v->nama_pasien; ?>" ><?php echo $v->nama_pasien; ?></option>
				<?php endif; ?>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label>nik :</label>
				<input type="text" name="nik" class="form-control" value="<?php echo $isi['nik']; ?>">	
			</div>
			<div class="form-group">
				<label>Tanggal Berkunjung :</label>
				<input type="date" name="tgl" class="form-control" value="<?php echo $isi['tanggal']; ?>">	
			</div>
			<div class="form-group">
				<label>Gender</label>
				<select name="gender" class="form-control" required>
				  <option value="">-- Pilih --</option>
				  	<?php if($isi['jenis_kelamin']== 'L'): ?>
				  <option value="L" selected>Laki-Laki</option>
				<?php elseif ($isi['jenis_kelamin'] == 'P') : ?>
				  	  <option value="P">Perempuan</option>
				<?php endif; ?>
				
				</select>
			</div>
			<div class="form-group">
				<label>Tujuan</label>
				<select name="poli" class="form-control" required>
				<option value="" selected disabled>-- Pilih Poli --</option>
					<?php if(!empty($poli)): ?>
					<?php foreach($poli as $v): ?>
						<?php if($isi['id_poli']==$v->id_poli): ?>
					<option value="<?php echo $v->id_poli; ?>" selected><?php echo $v->nama_poli; ?></option>
				<?php else : ?>
					<option value="<?php echo $v->id_poli; ?>"><?php echo $v->nama_poli; ?></option>
					<?php endif; ?>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<input type="submit" name="edit" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/jadwal/pasien'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>