<div class="row">
	<div class="col-md-8">
		<?= form_open('admin/jadwal/post_tambah_pasien'); ?>

		<div class="form-group">
				<label>No. Antrian :</label>
				<?php $x=0; ?>
				<input type="number" name="antri" class="form-control" value="000<?php foreach ($pasien as $key => $value) {
					echo $x+1;
				} ?>" readonly>	
			</div>
			<div class="form-group">
				<label>Nama Pasien :</label>
				<select name="id_pasien" class="form-control" >
					<option value="" selected disabled>-- Pilih --</option>
					<?php if(!empty($pasien)): ?>
					<?php foreach($pasien as $v): ?>
					<option value="<?php echo $v->nama_pasien; ?>"><?php echo $v->nama_pasien; ?></option>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="form-group">
				<label>nik :</label>
				<input type="text" name="nik" class="form-control">	
			</div>
			<div class="form-group">
				<label>Tanggal Berkunjung :</label>
				<input type="date" name="tgl" class="form-control">	
			</div>
			<div class="form-group">
				<label>Gender</label>
				<select name="gender" class="form-control" required>
				  <option value=" ">-- Pilih --</option>
				  <option value="L">Laki-Laki</option>
				  <option value="P">Perempuan</option>
				</select>
			</div>
			<div class="form-group">
				<label>Tujuan</label>
				<select name="poli" class="form-control" required>
				<option value="" selected disabled>-- Pilih Poli --</option>
					<?php if(!empty($poli)): ?>
					<?php foreach($poli as $v): ?>
					<option value="<?php echo $v->id_poli; ?>"><?php echo $v->nama_poli; ?></option>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/jadwal/pasien'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>