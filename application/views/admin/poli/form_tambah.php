<div class="row">
	<div class="col-md-4">
		<?= form_open('admin/poli/post_tambah'); ?>
			<div class="form-group">
				<label>Nama Poli :</label>
				<input type="text" name="nama_poli" class="form-control" required>	
			</div>
			<input type="submit" name="tambah" value="Simpan" class="btn btn-primary pull-left">
			<a href="<?php echo base_url('admin/poli/index'); ?>" class="btn btn-default pull-right">Kembali</a>
		<?= form_close(); ?>
	</div>
</div>