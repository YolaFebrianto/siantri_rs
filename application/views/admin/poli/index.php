<!-- Main content -->
<section class="content">
	<!-- Info boxes -->
	<div class="row">
		<div class="col-md-12">
			<a href="<?php echo base_url('admin/poli/form_tambah'); ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
			<?php if (count($poli)>0) { ?>
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama Poli</th>
						<th width="180">Opsi</th>
					</tr>
				</thead>
				<tbody>
			<?php
				foreach ($poli as $k => $v) {
			?>
				<tr>
					<td><?php echo ($k+1); ?></td>
					<td><?php echo $v->nama_poli; ?></td>
					<td>
						<a href="<?php echo base_url('admin/poli/form_edit/'.$v->id_poli); ?>" class="btn btn-info"><span class="fa fa-pencil"></span></a>
						<a href="<?php echo base_url('admin/poli/hapus/'.$v->id_poli); ?>" class="btn btn-danger"><span class="fa fa-trash-o"></span></a>
					</td>
				</tr>
			<?php
				}
			?>
				</tbody>
			</table>
			<?php
				} else {
					echo "<h4 style='text-align:center;'><i>Data Masih Kosong!</i></h4>";
				}
			?>
		</div>
	</div>
</section>
