<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// ROUTES ADMIN
$route['admin']						= 'admin/PenggunaController/index';
$route['admin/pengguna/(:any)']		= 'admin/PenggunaController/$1';
$route['admin/poli']				= 'admin/PoliController/index';
$route['admin/poli/(:any)']			= 'admin/PoliController/$1';
$route['admin/poli/(:any)/(:any)']	= 'admin/PoliController/$1/$2';
$route['admin/dokter']				= 'admin/DokterController/index';
$route['admin/dokter/(:any)']		= 'admin/DokterController/$1';
$route['admin/dokter/(:any)/(:any)']= 'admin/DokterController/$1/$2';
$route['admin/jadwal']				= 'admin/JadwalController/index';
$route['admin/jadwal/(:any)']		= 'admin/JadwalController/$1';
$route['admin/jadwal/(:any)/(:any)']= 'admin/JadwalController/$1/$2';
$route['admin/antrian']				= 'admin/AntrianController/index';
$route['admin/antrian/(:any)']		= 'admin/AntrianController/$1';
$route['admin/antrian/(:any)/(:any)']= 'admin/AntrianController/$1/$2';
$route['admin/pasien'] 				= 'admin/PasienController/index';
$route['admin/pasien/(:any)'] 		= 'admin/PasienController/$1';
$route['admin/pasien/(:any)/(:any)']= 'admin/PasienController/$1/$2';
// $route['pengguna/form-add']	 = 'PenggunaController/form_add';
// $route['pengguna/form-edit'] = 'PenggunaController/form_edit';

// ROUTES USER
$route['pasien'] 					= 'PasienController/index';
$route['pasien/(:any)'] 			= 'PasienController/$1';
$route['pasien/(:any)/(:any)'] 		= 'PasienController/$1/$2';
$route['jadwal'] 					= 'JadwalController/index';
$route['jadwal/(:any)'] 			= 'JadwalController/$1';
$route['jadwal/(:any)/(:any)'] 		= 'JadwalController/$1/$2';
$route['antrian'] 					= 'AntrianController/index';
$route['antrian/(:any)'] 			= 'AntrianController/$1';
$route['antrian/(:any)/(:any)'] 	= 'AntrianController/$1/$2';

$route['default_controller'] = 'PasienController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
